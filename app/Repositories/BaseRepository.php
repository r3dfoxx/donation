<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;
use Throwable;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Interfaces\BaseInterface;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
abstract class BaseRepository implements BaseInterface
{
    /**
     * @var Model
     */
    public $model;
    public $sortBy = 'id';
    public $sortOrder = 'desc';

    /**
     * Get all instances of model
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
     * Create a new record in the database
     *
     * @param array $data
     * @return model
     */
    public function create(array $data)
    {
        try {
            $model = $this->model->create($data);
        } catch (Throwable $cr) {
            report($cr);
        }
        return $model;
    }

    /**
     * Update record in the database and return status
     *
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function update(int $id, array $data): bool
    {
        try {
        $update = $this->model->where('id', $id)->update($data);
        } catch (Throwable $up) {
            report($up);
        }
        return $update;
    }

    /**
     * Remove record from the database
     *
     * @param int $id
     * @return boolean
     */
    public function destroy(int $id): bool
    {
        try {
            $this->model->destroy($id);
        }catch (Throwable $de) {
            report($de);
        }
        return true;
    }

    /**
     * Get the associated model
     *
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * Set the associated model
     *
     * @param $model
     * @return $this
     */

    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }
    public function paginate()
    {
    return $this->model->paginate(10);
    }
}



